package com.poc.common.service;

import com.poc.common.repo.MyFileRepo;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class MyFileService {

    private final MyFileRepo myFileRepo;

    public MyFileService(MyFileRepo myFileRepo) {
        this.myFileRepo = myFileRepo;
    }

    public Boolean create(String filePath) throws IOException {
        return myFileRepo.create(filePath);
    }

    public Boolean exists(String filePath) {
        return myFileRepo.exists(filePath);
    }
}
