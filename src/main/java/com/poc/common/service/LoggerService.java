package com.poc.common.service;

import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class LoggerService {

    private static Logger logger = Logger.getLogger(LoggerService.class.getName());

    public void info(String message){
        if(ObjectUtils.isEmpty(message)){
            message = "default log from common";
        }
        logger.log(Level.INFO, message);
    }

}
