package com.poc.common.service;

import com.poc.common.constants.MyConstants;
import com.poc.common.repo.MyRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MyService {

    @Autowired
    private final MyRepo myRepo;

    public MyService(MyRepo myRepo) {
        this.myRepo = myRepo;
    }

    public String returnString(){
        return myRepo.returnString();
    }
}
