package com.poc.common.repo;

import com.poc.common.constants.MyConstants;
import org.springframework.stereotype.Repository;

@Repository
public class MyRepo {

    public String returnString(){
        return MyConstants.PROJECT_NAME;
    }
}
