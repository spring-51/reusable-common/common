package com.poc.common.repo;

import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;

@Repository
public class MyFileRepo {
    public Boolean create(String filePath) throws IOException {
        File file = new File(filePath);
        file.getParentFile().mkdirs();
        return file.createNewFile();
    }

    public Boolean exists(String filePath){
        File file = new File(filePath);
        return file.exists();
    }
}
