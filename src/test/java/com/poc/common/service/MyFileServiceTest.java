package com.poc.common.service;

import com.poc.common.repo.MyFileRepo;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyFileServiceTest {

    @Test
    void create() {
        String filetPath = "./test/file1.txt";
        MyFileService fileService = new MyFileService(new MyFileRepo());
        assertDoesNotThrow(() -> fileService.create(filetPath));
    }
}